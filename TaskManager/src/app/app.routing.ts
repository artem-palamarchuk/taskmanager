﻿/* Core */
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Components */
import { LoginComponent } from './test.component';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: '**', redirectTo: '' }
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(appRoutes);