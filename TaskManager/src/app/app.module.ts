import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ROUTING } from './app.routing';

import { AppComponent } from './app.component';
import { LoginComponent } from './test.component';

@NgModule({
  imports: [BrowserModule, ROUTING ],
  declarations: [AppComponent, LoginComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
